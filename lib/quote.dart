import 'package:flutter/material.dart';

class Quote {
  String text;
  String author;

  Quote({required this.text, required this.author});

  Text toText() {
    return Text('$text - $author');
  }
}

var myQuote = Quote(author: '', text: '');
