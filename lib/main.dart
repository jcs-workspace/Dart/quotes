import 'package:flutter/material.dart';
import 'quote.dart';
import 'quote_card.dart';

void main() {
  runApp(MaterialApp(
    home: QuoteList(),
  ));
}

class QuoteList extends StatefulWidget {
  @override
  _QuoteListState createState() => _QuoteListState();
}

var myAppBar = AppBar(
  title: const Text('Awesome Quotes'),
  centerTitle: true,
  backgroundColor: Colors.redAccent,
);

class _QuoteListState extends State<QuoteList> {
  List<Quote> quotes = [
    Quote(
        author: 'Qsca Wilde',
        text: 'Be yourself; everyone else is already taken'),
    Quote(
        author: 'Qsca Wilde',
        text: 'I have nothing to declare except my genius'),
    Quote(
        author: 'Qsca Wilde',
        text: 'The truth is rarely pure and never simple'),
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.grey[200],
        appBar: myAppBar,
        body: Column(
          children: quotes.map((quote) {
            return QuoteCard(
                quote: quote,
                delete: () {
                  setState(() {
                    quotes.remove(quote);
                  });
                });
          }).toList(),
        ));
  }
}
